#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 19 22:06:37 2017

@author: atom
"""
import keras
import keras.preprocessing.image
from keras_retinanet.preprocessing.csv_generator import CSVGenerator
from keras_retinanet.models.resnet import custom_objects

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import csv

model = keras.models.load_model('./snapshots/resnet50_csv_01.h5', custom_objects=custom_objects)

image_data_gen = keras.preprocessing.image.ImageDataGenerator()

generator = CSVGenerator('train.csv','labels.csv',image_data_gen, batch_size=1)
index = 0
image = generator.load_image(index)

image = generator.preprocess_image(image)
image, scale = generator.resize_image(image)
annotations = generator.load_annotations(index)

index += 1

_,_,detections = model.predict_on_batch(np.expand_dims(image, axis=0))

predicted_labels = np.argmax(detections[0, :, 4:], axis=1)
scores = detections[0, np.arange(detections.shape[1]), 4 + predicted_labels]

detections[0, :, :4] /= scale


x_min=[]
y_min=[]
height=[]
width=[]

for idx, (label, score) in enumerate(zip(predicted_labels, scores)):
    if score < 0.5:
        continue
    b = detections[0, idx, :4].astype(int)
    x_min.append(b[0])
    y_min.append(b[1])
    width.append(b[2]-b[0])
    height.append(b[3]-b[1])
    
    

    
pd.DataFrame({"xmin":x_min, "y_min":y_min, "height":height, "width":width}).to_csv('Localized_coords.csv', index=False, header=False)

